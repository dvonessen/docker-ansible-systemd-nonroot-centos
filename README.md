# docker-ansible-systemd-nonroot-centos

This repository contains a build for CentOS 7 and 8 based Docker container, which enables testing for Ansible roles which uses roles with Systemd and root privileges.